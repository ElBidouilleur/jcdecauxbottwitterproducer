FROM ubuntu:latest

SHELL ["/bin/bash", "-c"] 

RUN apt-get update --fix-missing
RUN apt-get upgrade -y


RUN apt-get install git curl wget librdkafka-dev g++ gcc openssl unzip libcurl4-openssl-dev libssl-dev -y

RUN curl -fsSL https://xmake.io/shget.text | /bin/bash


RUN useradd dj
RUN mkdir -p /home/dj
RUN chown -R dj:dj /home/dj

RUN mkdir -p /tmp/
RUN chmod -R 777 /tmp/ 


USER dj

COPY . /home/dj/app

WORKDIR /home/dj/app

RUN curl -fsSL https://xmake.io/shget.text | /bin/bash

RUN /home/dj/.local/bin/xmake -y

CMD ["/home/dj/.local/bin/xmake", "run"]
