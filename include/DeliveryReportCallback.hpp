#pragma once

#ifndef KAFKA_PRODUCER_DELIVERY_REPORT_CALLBACK
#define KAFKA_PRODUCER_DELIVERY_REPORT_CALLBACK

#include <iostream>
#include <librdkafka/rdkafkacpp.h>

namespace kfp 
{

	class DeliveryReportCallback: public RdKafka::DeliveryReportCb
	{
		public:
			void dr_cb(RdKafka::Message &message);
	};

}
#endif // !KAFKA_PRODUCER_DELIVERY_REPORT_CALLBACK

