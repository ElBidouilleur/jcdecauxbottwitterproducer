#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <unistd.h>

#include <librdkafka/rdkafkacpp.h>
#include <curl/curl.h>

#include "../include/DeliveryReportCallback.hpp"

static volatile sig_atomic_t run = 1;

static void sigterm(int sig) {
	run = 0;
}

static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

int main(int argc, char** argv) {	
	std::string brokers;
	std::string topic;

	if (const char* brokerInEnv = std::getenv("jcdecaux_kafka_brokers")) {
		brokers = brokerInEnv;
	}
	else {
		brokers = argv[1];
	}

	if (const char* topicInEnv = std::getenv("jcdecaux_kafka_topic")) {
		topic = topicInEnv;
	}
	else {
		if (argc == 3) {
			topic = argv[2];
		}
		else {
			topic = argv[1];
		}
	}

	RdKafka::Conf* conf = RdKafka::Conf::create(RdKafka::Conf::CONF_GLOBAL);

	std::string errstr;

	if (conf->set("bootstrap.servers", brokers, errstr) != RdKafka::Conf::CONF_OK) {
		std::cerr << errstr << std::endl;
		exit(1);
	}

	signal(SIGINT, sigterm);
	signal(SIGTERM, sigterm);

	kfp::DeliveryReportCallback deliveryReportCallback;

	if (conf->set("dr_cb", &deliveryReportCallback, errstr) != RdKafka::Conf::CONF_OK) {
		std::cerr << errstr << std::endl;
		exit(1);
	}

	RdKafka::Producer* producer = RdKafka::Producer::create(conf, errstr);

	if (!producer) {
		std::cerr << "Failed to create producer: " << errstr << std::endl;
		exit(1);
	}

	delete conf;

	while (true)
	{
		std::string result;

		CURLcode code;

		CURL* connection = curl_easy_init();

		std::string apiKey;

		if (const char* apiKeyInEnv = std::getenv("jcdecaux_apikey")) {
			apiKey = apiKeyInEnv;
		}
		else {
			apiKey = "7907fd797d60aed936c94659c381860d46b91898";
		}


		std::string url = "https://api.jcdecaux.com/vls/v1/stations?apiKey=" + apiKey;

		code = curl_easy_setopt(connection, CURLOPT_URL, "https://api.jcdecaux.com/vls/v1/stations?apiKey=7907fd797d60aed936c94659c381860d46b91898");
		code = curl_easy_setopt(connection, CURLOPT_FOLLOWLOCATION, 1L);
		code = curl_easy_setopt(connection, CURLOPT_WRITEFUNCTION, WriteCallback);
		code = curl_easy_setopt(connection, CURLOPT_WRITEDATA, &result);

		code = curl_easy_perform(connection);
		curl_easy_cleanup(connection);

		if (code != CURLE_OK) {
			std::cerr << "Error " << code << std::endl;
			exit(1);
		}


	retry:
		RdKafka::ErrorCode err = producer->produce(
			/* Topic name */
			topic,
			/* Any Partition: the builtin partitioner will be
			 * used to assign the message to a topic based
			 * on the message key, or random partition if
			 * the key is not set. */
			RdKafka::Topic::PARTITION_UA,
			/* Make a copy of the value */
			RdKafka::Producer::RK_MSG_COPY /* Copy payload */,
			/* Value */
			const_cast<char*>(result.c_str()), result.size(),
			/* Key */
			NULL, 0,
			/* Timestamp (defaults to current time) */
			0,
			/* Message headers, if any */
			NULL,
			/* Per-message opaque value passed to
			 * delivery report */
			NULL);

		if (err != RdKafka::ERR_NO_ERROR) {
			std::cerr << "% Failed to produce topic " << topic << ": " << RdKafka::err2str(err) << std::endl;

			if (err == RdKafka::ERR__QUEUE_FULL) {
				producer->poll(1000);
				goto retry;
			}
		}
		else {
			std::cerr << "& Enqueued message (" << result.size() << " bytes) for topic " << topic << std::endl;
		}

		producer->poll(0);

		std::cerr << "% Flushing final messages..." << std::endl;

		producer->flush(60 * 1000 /* wait for max 10 seconds */);

		if (producer->outq_len() > 0) {
			std::cerr << "% " << producer->outq_len() << " meessage(s) were not delivered" << std::endl;
		}

		sleep(10);
	}
	delete producer;

	return 0;
}